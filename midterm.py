dict_p = {"name" : "?", "missile" : "미사일", "ult" : "궁극탄"}
enemy = 1
ult = 1
death = 0

print("코드네임을 입력하세요 : ")
dict_p["name"] = input()

print("\n쉬이이이이이잉---")
input()
print(dict_p["name"], "전투원.")
input()
print("이번 미션은 북쪽에 있는 적의 거처까지 무사히 돌파하는 것이다.")
input()
print("만약 적의 공격에 대응하지 못하면..")
input()
print("죽음뿐이다. 절대 방심하지 말도록.")
input()
print("바람이 서쪽으로 불고 있군..")
input()
print("!!WARNING!! !!WARNING!! !!WARNING!!")
input()
print("위험 구역 진입.", dict_p["name"], "전투원 준비하라.\n")
print("-MISSION START-")
input()

def print_enemy(n):
    if n == 1:
        print("적 비행체 발생")
    elif n == 2:
        print("두 번째 적 비행체 발생")
    elif n == 3:
        print("세 번째 적 비행체 발생")

for i in range(3):
    print_enemy(enemy)
    enemy += 1
    
    if ult == 1:
        list_p = ["1. 미사일 발사", "2. 오른쪽 이동", "3. 왼쪽 이동", "4. 궁극탄(한번만 사용가능)"]
    elif ult == 0:
        list_p = ["1. 미사일 발사", "2. 오른쪽 이동", "3. 왼쪽 이동"]

    print(list_p)

    number = input("행동을 선택하세요 : ")
    print("")

    if number == "1":
        print(list_p[0], "선택\n")
        print(dict_p["name"], "는(은)" , dict_p["missile"], "을 발사했다.")
        input()
        print("띠-띠-띠 목표물 포착.")
        input()
        print("푸슈우우웅... 콰과강!!!")
        input()
        print("적 기체 격추")
        print("--------------------------------------")
    elif number == "2":
        print(list_p[1], "선택\n")
        print(dict_p["name"], "는(은) 오른쪽으로 이동했다.")
        input()
        print("쉬이이이......휘이이잉!!!")
        input()
        print("바람의 영향 때문에 가까스로 스쳐지나갔다.")
        print("--------------------------------------")
    elif number == "3":
        print(list_p[2], "선택\n")
        print(dict_p["name"], "는(은) 왼쪽으로 이동했다.")
        input()
        print("쉬이이이...휘~융")
        input()
        print("적 기체 회피")
        print("--------------------------------------")
    elif number == "4":
        print(list_p[3], "선택\n")
        print(dict_p["name"], "는(은)", dict_p["ult"], "을 발사했다.")
        input()
        print("철컥.. 위이잉.. 철컥.. 목표물 정조준.")
        input()
        print("띠-띠-띠-띠이이이이이이이-띵!")
        input()
        print("쿠구구구구구.... 푸슈우우우웅..........")
        input()
        print("......ㅋ쿠우우우우우우우우우우우우우우우우우우우우우와아아아아아아아아아아아아아아아아아아아아아아아아아앙!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        input()
        print("!!")
        input()
        print("엄청난 굉음과 함께 적이 흔적도 없이 사라졌다.")
        input()
        print("주변의 구름들까지 크게 흩어져 바다가 보인다. 이것이.. 궁극탄..?")
        print("--------------------------------------")
        ult -= 1
    else:
        print("슈우우우웅.........")
        input()
        print("콰과가가가가강!!!!!!!!!!!!!!!!!!!!!!!!!")
        input()
        print("······································")
        input()
        print("전투에서의 방심은 죽음입니다.\n")
        print("--------------------------------------")
        death += 1
        break
          
if death >= 1:
    print("-MISSION FAILURE-\n")
    print(dict_p["name"], "전투원 사망.")
    input()
elif death <= 0:
    print("-MISSION SUCCESS-\n")
    print("위험구역을 무탈하게 통과했다.")
    input()